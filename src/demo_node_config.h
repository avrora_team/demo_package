#ifndef DEMO_NODE_CONFIG_H
#define DEMO_NODE_CONFIG_H

#include <ros/ros.h>

class DemoNodeConfig
{

public:
	void init()
	{
		ros::NodeHandle parameter_node_handle("~");
		parameter_node_handle.param<double>("coeff2", coeff2, 0.1);
		parameter_node_handle.param<std::vector<double>>("covariance_matrix", covariance_matrix, {10.0, 0.0, 0.0, 3.0, 10.0, 0.0, 0.0, 2.0, 10.0});
		parameter_node_handle.param<double>("coeff1", coeff1, 100.0);
		parameter_node_handle.param<std::string>("port", port, "/dev/ttyUSB0");
	}

	double coeff2 = 0.1;
	std::vector<double> covariance_matrix = {10.0, 0.0, 0.0, 3.0, 10.0, 0.0, 0.0, 2.0, 10.0};
	double coeff1 = 100.0;
	std::string port = "/dev/ttyUSB0";

};

extern DemoNodeConfig demo_node_config;

#endif
