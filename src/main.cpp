#include <ros/ros.h>

#include "demo_node_config.h"

int main(int argc, char *argv[])
{
  // Инициализация ROS
  ros::init(argc, argv, "demo_node");
  ros::NodeHandle node_handle;

  // Инициализация конфигурационных файлов
  demo_node_config.init();


  // Доступ к параметрам конфигурации
  std::cout<<demo_node_config.port << std::endl;

  ros::Rate rate(0.5);
  while (ros::ok())
  {
    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
